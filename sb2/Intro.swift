//
//  Intro.swift
//  sb2
//
//  Created by Alain on 16-10-06.
//  Copyright © 2016 Production sur support. All rights reserved.
//

import UIKit

class Intro: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(Intro.passerAuMenuPrincipal), userInfo: nil, repeats: false)
    } // viewDidLoad()
    
    func passerAuMenuPrincipal(){
        performSegue(withIdentifier: "versMenuPrincipal", sender: self)
    } // passerAuMenuPrincipal
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
