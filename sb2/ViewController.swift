//
//  ViewController.swift
//  sb2
//
//  Created by Alain on 16-10-03.
//  Copyright © 2016 Production sur support. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func retourner02(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func retourner(_ sender: AnyObject) {
        print("retourner")
 
        
        // Retour au début
        _ = self.navigationController?.popToRootViewController(animated: true)
        
        // Retour au précédent
        _ = self.navigationController?.popViewController(animated: true)
        /*
        dismiss(animated: true, completion: { print("dismiss: completion")})
        */
 }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

